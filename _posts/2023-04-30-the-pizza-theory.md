---
layout: post
title: The Pizza Theory
published: false
---

My mind is like a pizza. Little bits of data on the toppings that have a tendacy to decay in a way. Some toppings last longer than others. This thought comes to me while thinking of how it relates to a computer's hard drive. The pizza is cut into slices, each slice is accessed by my environment. Each slice is like a partition on the hard drive. Each partition contains data for that environment.

It explains why I can't remember certine things until I'm in that environment. Also why some information fades away untill someone reminds me, much like parity data in a computer's disk array. This new information comes in and repairs the faded data and suddenly, I can access it. 