---
title: I think I inadvertently angered someone...
layout: post
---

Idk how much of this is aspie related but here goes...

Last weekend I went to the hardware store with dad to get somethings and upon leaving I was trying to reverse out, as you do, and found I had no room to complete the turn and I happen to notice the parking spot in front of me was open so I started to pull forward only to find some guy let his buggy roll right in front of me where I had been just a few seconds ago. He was loading some sort of plant in his car so I chirp the horn to get his attention and perhaps rudely gestured to the buggy to get him to move it. Without realizing it, this angered the little man but he move the buggy anyway and I took off. That was that. I'm glad he didn't attack because idk what I'd have done if he did.